FROM golang as builder
RUN go get -d -v github.com/docker/docker/client
RUN go get -d -v golang.org/x/sys/unix
RUN go get -d -v github.com/shirou/gopsutil
WORKDIR /app
COPY . /app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM scratch
WORKDIR /app
COPY --from=builder /app/app .
EXPOSE 7000
ENTRYPOINT ["./app"]
CMD ["-command-port=7000", "-log-socket=sock/logs.sock", "-output-dir=logs"]
