package main

import (
	"flag"
	"time"

	"./manager"
	"github.com/docker/docker/client"
)

func main() {
	var (
		outputDir   = flag.String("output-dir", "logs/", "Directory where logs are written")
		logSocket   = flag.String("log-socket", "sock/logs.sock", "UNIX socket for container log input")
		commandPort = flag.Uint("command-port", 7000, "TCP port for command interface")
	)

	flag.Parse()

	cli, err := client.NewEnvClient()
	if err != nil {
		panic(err)
	}

	manager := manager.New(cli, manager.Options{
		OutputDir:         *outputDir,
		LogSocket:         *logSocket,
		CommandPort:       *commandPort,
		HostStatsInterval: 1 * time.Second, // TODO: make command-line argument
	})
	manager.Run()
}
