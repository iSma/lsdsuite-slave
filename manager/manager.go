package manager

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path"
	"time"

	"./commands"
	"./events"
	"./host"
	"./logs"
	"./stats"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
)

// TODO: add chan direction in all declarations (e.g. New(out chan<- Entry))
// TODO: change Entry to Message

type Manager struct {
	Cli      *client.Client
	Options  Options
	Channels channels
	LogFile  string
	Hostname string
}

type channels struct {
	Output   chan string
	Logs     chan logs.Entry
	Stats    chan stats.Entry
	Events   chan events.Entry
	Host     chan host.Entry
	Commands chan commands.Command
}

type Options struct {
	OutputDir         string
	LogSocket         string
	CommandPort       uint
	HostStatsInterval time.Duration
}

// ----------------------------------------------------------------

func New(cli *client.Client, options Options) *Manager {
	return &Manager{
		Cli:     cli,
		Options: options,
		Channels: channels{
			// TODO: what chans need to be buffered?
			// If the buffer values are too low, we risk deadlocks
			Output:   make(chan string, 1),
			Logs:     make(chan logs.Entry, 1),
			Stats:    make(chan stats.Entry, 1),
			Events:   make(chan events.Entry, 1),
			Host:     make(chan host.Entry, 1),
			Commands: make(chan commands.Command, 1),
		},
	}
}

// ----------------------------------------------------------------

func (m *Manager) Run() {
	log.Printf("START: MANAGER: %+v\n", m.Options)
	if err := m.setLogFile(""); err != nil {
		log.Fatalf("FATAL: OUT: can't open '%s/default.log': %v", m.Options.OutputDir, err)
	}

	hostname, err := os.Hostname()
	if err != nil {
		log.Fatalf("FATAL: MANAGER: can't get hostname\n")
	}
	m.Hostname = hostname

	go commands.New(m.Options.CommandPort, m.Channels.Commands).Run()
	go events.New(events.Options{}, m.Cli, m.Channels.Events).Run()
	go logs.New(m.Options.LogSocket, m.Channels.Logs).Run()
	go host.New(m.Options.HostStatsInterval, m.Channels.Host).Run()

	go m.dispatch()

	for line := range m.Channels.Output {
		m.log(line)
	}
}

func (m *Manager) dispatch() {
	for {
		select {
		case cmd := <-m.Channels.Commands:
			m.handle(cmd)

		case host := <-m.Channels.Host:
			v, _ := json.Marshal(host)
			m.Channels.Output <- fmt.Sprintf("%v [HOST]\t%s\t%s",
				time.Now().Format(time.RFC3339Nano),
				m.Hostname,
				string(v))

		case stats := <-m.Channels.Stats:
			v, _ := json.Marshal(stats.Stats)
			m.Channels.Output <- fmt.Sprintf("%v [STATS]\t%s\t%s",
				stats.Stats.Read.UTC().Format(time.RFC3339Nano),
				stats.ID,
				string(v))

		case event := <-m.Channels.Events:
			if event.Type != "container" {
				// TODO: do we need these events?
				// Let's keep them for now
				// break
			}

			if event.Status == "start" {
				// Only collect stats for marked containers
				if _, ok := event.Actor.Attributes["org.lsdsuite.stats"]; ok {
					// A new container appeared
					go stats.New(event.Actor.ID, m.Cli, m.Channels.Stats).Run()
				}
			}

			v, _ := json.Marshal(event)
			m.Channels.Output <- fmt.Sprintf("%v [EVENT]\t%s\t%s",
				time.Unix(0, event.TimeNano).Format(time.RFC3339Nano),
				event.Actor.ID,
				string(v))

		case logs := <-m.Channels.Logs:
			m.Channels.Output <- fmt.Sprintf("%v [LOG]\t%s\t%s",
				logs.Time.UTC().Format(time.RFC3339Nano),
				logs.ID,
				logs.Message)
		}
	}
}

func (m *Manager) setLogFile(file string) error {
	if file == "" {
		file = "default.log"
	}

	file = path.Join(m.Options.OutputDir, file)

	// Check if file is writable
	f, err := os.OpenFile(file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)
	if err != nil {
		return err
	}

	defer f.Close()

	if _, err = f.Write([]byte{}); err != nil {
		return err
	}

	m.LogFile = file
	return nil
}

func (m *Manager) log(line string) {
	f, err := os.OpenFile(m.LogFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)
	if err != nil {
		log.Printf("ERROR: OUT: can't open '%s': %v", m.LogFile, err)
		return
	}

	defer f.Close()

	if _, err = f.Write([]byte(line + "\n")); err != nil {
		log.Printf("ERROR: OUT: can't write to '%s': %v", m.LogFile, err)
	}
}

func (m *Manager) handle(cmd commands.Command) {
	log.Printf("CMD: %+v\n", cmd)
	switch cmd.Command {
	case "status":
		cmd.Response <- resp("ok", m.Hostname)

	case "log": // Switch logging output to `params.file` ("default.log" if empty)
		file := cmd.Params["file"]
		if err := m.setLogFile(file); err != nil {
			cmd.Response <- resp("err", err.Error())
			return
		}

		cmd.Response <- resp("ok", "")
		log.Printf("OUT: '%s'", m.LogFile)

	case "kill": // Kill container `params.id` with signal `params.signal`
		id := cmd.Params["id"]
		if id == "" {
			cmd.Response <- resp("err", "params.id required")
			break
		}

		// TODO: context???
		if err := m.Cli.ContainerKill(context.Background(), id, cmd.Params["signal"]); err != nil {
			cmd.Response <- resp("err", err.Error())
			break
		}

		cmd.Response <- resp("ok", "")

	case "pause": // Pause container `params.id`
		id := cmd.Params["id"]
		if id == "" {
			cmd.Response <- resp("err", "params.id required")
			break
		}

		// TODO: context???
		if err := m.Cli.ContainerPause(context.Background(), id); err != nil {
			cmd.Response <- resp("err", err.Error())
			break
		}

		cmd.Response <- resp("ok", "")

	case "unpause": // Unpause container `params.id`
		id := cmd.Params["id"]
		if id == "" {
			cmd.Response <- resp("err", "params.id required")
			break
		}

		// TODO: context???
		if err := m.Cli.ContainerUnpause(context.Background(), id); err != nil {
			cmd.Response <- resp("err", err.Error())
			break
		}

		cmd.Response <- resp("ok", "")

	case "pull": // Pull image `params.image`
		image := cmd.Params["image"]
		if image == "" {
			cmd.Response <- resp("err", "params.image required")
			break
		}

		// TODO: context???
		// TODO: ImagePullOptions?
		_, err := m.Cli.ImagePull(context.Background(), image, types.ImagePullOptions{})
		if err != nil {
			cmd.Response <- resp("err", err.Error())
			break
		}

		// We don't care about the output for now
		// It seems to be a sequence of JSON objects in the form {"status":"..."[, "id": "...."]}
		/*
			body, err := ioutil.ReadAll(res)
			if err != nil {
				cmd.Response <- resp("err", err.Error())
				break
			}

			fmt.Println(string(body))
		*/
		cmd.Response <- resp("ok", "")

	case "mark":
		msg := cmd.Params["msg"]
		if msg == "" {
			cmd.Response <- resp("err", "params.msg required")
			break
		}

		m.Channels.Output <- fmt.Sprintf("%v [MARK]\t%s\t%s",
			time.Now().Format(time.RFC3339Nano),
			m.Hostname,
			msg)

		cmd.Response <- resp("ok", "")

	default:
		cmd.Response <- resp("err", fmt.Sprintf(`unknown command "%s"`, cmd.Command))
	}

}

func resp(status string, message string) commands.Response {
	return commands.Response{status, message}
}
