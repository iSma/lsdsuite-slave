package stats

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
)

type Manager struct {
	id  string
	cli *client.Client
	out chan Entry
}

type Entry struct {
	ID    string
	Stats Stats
}

type Stats types.Stats

func New(id string, cli *client.Client, out chan Entry) *Manager {
	return &Manager{
		id:  id,
		cli: cli,
		out: out,
	}
}

func (m *Manager) Run() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	stream, err := m.cli.ContainerStats(ctx, m.id, true)
	if err != nil {
		// TODO
		log.Printf("ERROR: stats[%s]: %v\n", m.id, err)
		return
	}
	defer stream.Body.Close()

	log.Printf("START: stats[%s]\n", m.id)
	defer log.Printf("STOP: stats[%s]\n", m.id)

	for stats := (Stats{}); ; {
		if err = json.NewDecoder(stream.Body).Decode(&stats); err != nil {
			if err == io.EOF {
				return
			}

			// Ignore parsing errors
			log.Printf("ERROR: stats[%s]: JSON: %v\n", m.id, err)
			return
		}

		// "zero" time indicates absence of data, which happens either when the container
		// hasn't started yet or was stopped. We ignore this
		if stats.Read == (time.Time{}) {
			continue
		}

		m.out <- Entry{
			ID:    m.id,
			Stats: stats,
		}
	}
}
